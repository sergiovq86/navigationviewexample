package com.tema3.navigationdrawerexample;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;
import com.tema3.navigationdrawerexample.fragments.CameraFragment;
import com.tema3.navigationdrawerexample.fragments.GalleryFragment;
import com.tema3.navigationdrawerexample.fragments.HomeFragment;
import com.tema3.navigationdrawerexample.fragments.MailFragment;
import com.tema3.navigationdrawerexample.fragments.SlideshowFragment;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.drawerLayout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences preferences = getSharedPreferences("mypreferencias", MODE_PRIVATE);
        String mail = preferences.getString("email", "");
        if (mail.isEmpty()) {

        } else {
            Toast.makeText(this, "Este es el email" + mail, Toast.LENGTH_SHORT).show();
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                navigateToFragment(id);
                //item.setChecked(true);
                return true;
            }
        });

//        View header = navigationView.getHeaderView(0);
//        TextView correo = header.findViewById(R.id.textView);
//        correo.setText("info@escuelaestech.es");
//        correo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(MainActivity.this, "Enviar correo", Toast.LENGTH_SHORT).show();
//            }
//        });

        navigateToFragment(R.id.nav_home);
        navigationView.setCheckedItem(R.id.nav_home);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void navigateToFragment(int itemId) {
        Fragment fragment;
        String title;

        switch (itemId) {
            default:
                fragment = new HomeFragment();
                title = "Home";
                break;

            case R.id.nav_home:
                fragment = new HomeFragment();
                title = "Home";
                break;

            case R.id.nav_gallery:
                fragment = new GalleryFragment();
                title = "Gallery";
                break;

            case R.id.nav_camera:
                fragment = new CameraFragment();
                title = "Camera";
                break;

            case R.id.nav_slideshow:
                fragment = new SlideshowFragment();
                title = "SlideShow";
                break;

            case R.id.nav_mail:
                fragment = new MailFragment();
                title = "Email";
                break;
        }
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.frame_layout, fragment);
        //transaction.addToBackStack(null);
        transaction.commit();

        setTitle(title);
        drawerLayout.closeDrawer(GravityCompat.START);
    }
}